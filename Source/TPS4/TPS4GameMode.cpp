// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS4GameMode.h"
#include "TPS4PlayerController.h"
#include "TPS4Character.h"
#include "UObject/ConstructorHelpers.h"

ATPS4GameMode::ATPS4GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS4PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}