// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS4.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TPS4, "TPS4" );

DEFINE_LOG_CATEGORY(LogTPS4)
 