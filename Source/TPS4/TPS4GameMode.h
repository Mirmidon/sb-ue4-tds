// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS4GameMode.generated.h"

UCLASS(minimalapi)
class ATPS4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS4GameMode();
};



